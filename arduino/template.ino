#include <ArduinoJson.h>
#include <SimpleTimer.h>
#include <AccelStepper.h>


/*
Events:
  Recived:
    $stop: it will stop experiment
    $status: it asks for the current state
  Emitted: 
    $recived -> it notifies the data were recived
    $status, ok -> response status of the system is ok
    $status, error:msg  -> response system has some error
*/

/*------------------------------------- PINS AREA -------------------------------------------*/
const int motorInterfaceType = 1;
const int dirPin = 6;
const int stepPin = 7;
const int enablePin = 8;
const int lightPin = 9;

/*-------------------------------- VARIABLES FOR SERIAL -------------------------------------*/

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

/*--------------------------------- VARIABLES ÁREA -------------------------------------------*/
// Initialize json object
StaticJsonDocument<512> doc;

bool playState = false;
bool dirState = true;
bool lightState = false;

bool speedReady = true;
int stepSpeedSetPoint = 20;
int stepSpeed = 0;
int stepsMeasured = 0;
const int stepJump = 20;
bool isInUse = false;

SimpleTimer stopTimer(60000);
int maxTime = 10; // Minutes
int minutes = 0; 


AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);
/*------------------------------------SEND/READ INFO-----------------------------------------*/

void sendVariables(){
  doc["PlayState"] = playState;
  doc["DirState"] = dirState;
  doc["LightState"] = lightState;
  doc["Steps"] = stepSpeedSetPoint;

  serializeJson(doc, Serial);  
  Serial.println();
}

void dataRecived(){
  Serial.println("$recived");
}

void configureExperiment(){
  speedReady = false;
  isInUse = true;
  minutes = 0;
  if(!playState)
    stepSpeed = 0;
  stopTimer.reset();
}

void readVariables(String data){
  
  DeserializationError error = deserializeJson(doc, data);
  
  if(error){
    // Serial.println(error.f_str());
    return;
  }
  // Set Variables
  playState = doc["PlayState"];
  dirState = doc["DirState"];
  lightState = doc["LightState"];
  stepSpeedSetPoint = doc["Steps"];

  configureExperiment();
  // Notify the data were recived
  dataRecived();
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    inputString += inChar;
    
    if (inChar == '\n') {
      
      if(inputString[0] == '$'){
        readEvents(inputString);
      }else{
        readVariables(inputString);
      }
      
      stringComplete = true;
      inputString = "";
    }
  }
}

void readEvents(String data){
    String event = data.substring(1);
    event.trim();
    if(event == "stop"){
      stop();
    }
    if(event == "status"){
      areYouOk();
    }
}

/*----------------------------------------- EVENTS ----------------------------------------------*/

void autoStop(){

  if(stopTimer.isReady()){
    minutes++;
    stopTimer.reset();
  }

  if(minutes >= maxTime){
    stop();
    minutes = 0;
    isInUse = false;
  }

}

/*------------------------------------- LOGIC FUNCTIONS -----------------------------------------*/
SimpleTimer speedControlTimer(2000);

void speedWasSet() {
  Serial.println("$speedReady");  
}

void speedControl(){
  //
    if(speedControlTimer.isReady()){
      setSoftSpeed();
      speedControlTimer.reset();
    }
 
}

void setSoftSpeed(){
  //
   if(speedReady == false && playState == true){
      // setteo de subida
      if(stepSpeed < stepSpeedSetPoint){ //si velocidad actual es menor que el setpoint
       stepSpeed += stepJump; // incrementar velocidad actual N pasos
 
       if(stepSpeed >= stepSpeedSetPoint){ // Si se superó o igualó el setpoint
        stepSpeed = stepSpeedSetPoint; // Settear velocidad en el setPoint
        speedReady = true; // Velocidad Setteada
        speedWasSet();
        return;
       }
  
     }  
      
      // setteo de bajada
      if(stepSpeed >= stepSpeedSetPoint){
        stepSpeed -= stepJump;

        if(stepSpeed <= stepSpeedSetPoint){
          stepSpeed = stepSpeedSetPoint;
          speedReady = true;
          speedWasSet();
          return;
        }  
     }
     
   }    
}

void stop(){
  lightState = false;
  playState = false;
  speedReady = true; 
  stepSpeed=0;
  sendVariables();
  stopTimer.reset();
  //Serial.println("$stopped");
}

void lightControl(){
 digitalWrite(lightPin, lightState); 
}

void motorControl(){
  // Enable or disable Motor
  digitalWrite(enablePin, !playState);
 
  //choose direction
  if(!dirState)
    stepper.setSpeed(stepSpeed);
    
    if(dirState)
    stepper.setSpeed(-stepSpeed);
    
  //move motor  
   stepper.runSpeed();  
}

void sensorReading(){
  //
}

void areYouOk(){
  bool ok = true;
  String error = "";
  
  // check if are there any error on the system
  if(ok){
    Serial.println("$status,ok");
  }else{
    Serial.println("$status,error: " + error);
  }
}

/*------------------------------------------ SET UP -----------------------------------------------*/

void configurePins(){
  pinMode(lightPin, OUTPUT);
  digitalWrite(lightPin, LOW);
}

void setup() {
  Serial.begin(9600);
  inputString.reserve(512);
  configurePins();

    //
  stepper.setMaxSpeed(200);
  stepper.setAcceleration(400);
}

void loop() {
  autoStop();
  speedControl();
  lightControl();
  motorControl();

}
