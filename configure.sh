REPO_BASE=https://Hikki12@bitbucket.org/Hikki12/python-mockupio.git
REPO_FOLDER=python-mockupio
VIRTUAL_ENV_NAME=venv

folder_exists(){
    if [ -d "$1" ]; then
        #echo "Folder $1 exists!" 
        return $(true)
    else
        #echo "Folder $1 doesn't exists!" 
        return $(false)
    fi
}

set_env(){
    source $VIRTUAL_ENV_NAME/bin/activate
}58

create_env(){
    if folder_exists $VIRTUAL_ENV_NAME; then 
        return
    else
        python3 -m venv venv
    fi
}


configure_env(){
    create_env
    set_env
}

clone_repo(){
    echo "Configuring repo..."
    git submodule update --init --recursive --force
}

install_library(){
    echo "Installing library..."
    cd python-mockupio
    python setup.py install
    cd ..
}

configure_repo(){
    clone_repo
    install_library
}



configure_env
configure_repo