import cv2
import numpy as np
import time

# Kernel Size
ksize = (7, 7)
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize)

# Blue color limits
blue = np.array([[70, 50, 130], [120, 255, 255]])

# green color limits
green = np.array([[30, 12, 10], [100, 255, 255]])

# yellow color limits
yellow = np.array([[13, 7, 150], [48, 255, 255]])

color_blue = (100, 50, 200)
color_green = (60, 215, 153)
color_yellow = (20, 200, 240)

# Real dimensions
R1 = 3  # cm
R2 = 6  # cm
R3 = 9  # cm


def get_position(img=None, origin=None, track_color_range=None, color_contour=None, draw_contour=False):
    # Change color space
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # filter out everything that is not my chosen color
    mask = cv2.inRange(img_hsv, track_color_range[0], track_color_range[1])
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # filter noise
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    # Obtain contours of my tracked object
    contour, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # _,contour,_ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if draw_contour:
        cv2.drawContours(img, contour, -1, color_contour, 2)  # draw the contour of my object

    x0, y0 = origin
    # calculate position with the area of the object
    try:
        max_contour = max(contour, key=cv2.contourArea)  # choose the largest element
        moments = cv2.moments(max_contour)  # calculate moments
        x = (moments['m10'] / moments['m00']) - x0  # x position
        y = y0 - (moments['m01'] / moments['m00'])  # y position
    except:
        x = x0
        y = y0

    position = (x, y)
    return position


def draw_info_function(img, origin, position, draw_angle=False):
    x0, y0 = origin
    x, y = position

    # Plot axis X and Y
    cv2.line(img, (0, y0), (img.shape[1], y0), (100, 80, 10), 1)
    cv2.line(img, (x0, 0), (x0, img.shape[0]), (100, 80, 10), 1)

    # draw the center of the image
    cv2.circle(img, (x0, y0), 8, (100, 80, 10), -1)

    # Draw a radio vector
    x_pix, y_pix = int(x + x0), int(abs(y - y0))
    #cv2.arrowedLine(img, (x0, y0), (x_pix, y_pix), (100, 80, 10), 2, line_type=8, tipLength=0.3)

    # Draw position of the particle
    #cv2.circle(img, (x_pix, y_pix), 2, (180, 150, 150), -1)

    if draw_angle:
        try:
            angle = np.arctan2(y, x)
            angle *= (180 / np.pi)
            if angle < 0:
                angle += 360
        except:
            angle = 0
        # Display Angle
        cv2.putText(img, "{:.2f}".format(angle), (10, 2 * y0 - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (100, 220, 250), 2,
                    cv2.LINE_AA)


def image_adjust(img, a=0.8, b=1, sat=1.1):
    """It adjust contrast, brightness and saturation of a image
    :param img: image array
    :param a: contrast
    :param b: brightness
    :param sat: saturation
    :return: img: image array processed
    """
    img = cv2.convertScaleAbs(img, alpha=a, beta=b)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV).astype("float32")
    h, s, v = cv2.split(hsv)
    s = s * sat
    s = np.clip(s, 0, 255)
    hsv = cv2.merge([h, s, v])
    img = cv2.cvtColor(hsv.astype("uint8"), cv2.COLOR_HSV2BGR)
    return img

def crop_image(img, xratio=0.7, yratio=1):
    center = np.array(img.shape)/2
    x = int(center[1])
    y = int(center[0])
    a = x - int(xratio*x)
    b = x + int(0.65*x)
    c = y - int(0.87*y)
    d = y + int(yratio*y)
    new = img[c:d, a:b]
    return new


def preprocess(img, **kwargs):
    img = image_adjust(img, a=0.8,b=1, sat=1.3)
    img = crop_image(img)
    return img

def track(img=None, *args, **kwargs):
    #radius = kwargs["vars"]["radius"]
    radius = 3
    offset = [-1 , -1]
    if radius == 1:
        track_color_range = blue
        track_color = color_blue
    elif radius == 2:
        track_color_range = yellow
        track_color = color_yellow
    elif radius == 3:
        track_color_range = green
        track_color = color_green
    else:
        raise Exception("Not valid radius index! Please check it.")
    origin = (int(img.shape[1]/2 - offset[0]), int(img.shape[0]/2) - offset[1])
    # Get current position of the particle
    position = get_position(img=img, origin=origin, track_color_range=track_color_range, color_contour=track_color)
    draw_info_function(img=img, origin=origin, position=position)
    return position

