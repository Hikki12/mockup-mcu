
import sys
import os
from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QApplication
import logging
import json

sys.path.append(os.path.join(os.path.dirname(__file__), '../python-mockupio'))
from mockupio import MockupClient
from options import *
from color_tracker import track, preprocess
from utils import toPixmap

ui_path = os.path.dirname(os.path.abspath(__file__))
ui_file = os.path.join(ui_path, 'GUI.ui')

logging.basicConfig(level=logging.DEBUG)
logging.getLogger(__name__).setLevel(logging.ERROR)


class Mockup(QMainWindow):
    """"Mockup.
    Add some description here.
    """
    def __init__(self, 
            camera_options, 
            serial_options, 
            socketio_options, 
            extra_options,
            **kwargs):
        super().__init__()
        uic.loadUi(ui_file, self)

        # SET VARIABLES ----------------------------------------------------------------------
        self.variables = {
            "PlayState": False,
            "LightState": False,
            "DirState": False,
            "Steps": 0
        }
        # INITIALIZE CLIENT ------------------------------------------------------------------

        self.client = MockupClient(**camera_options, **serial_options, **socketio_options, **extra_options)

        # MOCKUP CALLBACKS --------------------------------------------------------------------

        self.client.on('camera-image-ready', self.show_image)
        self.client.on('camera-record-end', self.record_end)

        self.client.on('serial-connection-event', self.handle_serial_connection_status)
        self.client.on('serial-incoming-data', self.recive_data_serial)

        self.client.on('socketio-connection-event', self.handle_socketio_connection_status)
        self.client.on('socketio-request-updates', self.response_to_server)
        self.client.on('socketio-incoming-data', self.recive_data_socketio)
        self.client.on('socketio-stop-event', self.handle_stop_event_socketio)
        self.client.on('socketio-busy', self.handle_busy_event)

        self.client.on('error', self.handle_errors)
        
        # MULTIPROCESSING CALLBACKS ----------------------------------------------------------

        # self.params = self.client.obtain_dict()
        # self.params["color"] = 1
        self.client.camera.set_callback('preprocess', preprocess)
        # self.client.camera.set_callback('after-read', track, **self.params)

        # START  MOCKUP CLIENT ---------------------------------------------------------------------

        self.client.start(camera=True, serial=True, socketio=True)

        # READ GUI EVENTS -------------------------------------------------------------------------
        self.PlayState.clicked.connect(lambda: self.gui_events(id='PlayState', value=self.PlayState.isChecked()))
        self.LightState.clicked.connect(lambda: self.gui_events(id='LightState', value=self.LightState.isChecked()))
        self.DirState.clicked.connect(lambda: self.gui_events(id='DirState', value=self.DirState.isChecked()))
        self.Steps.sliderReleased.connect(lambda: self.gui_events(id='Steps', value=self.Steps.value()))

        self.Steps.valueChanged.connect(self.update_slider_label) 
        self.closeBtn.clicked.connect(self.close)

    def update_slider_label(self):
        speedStr = "{:.2f}".format(self.Steps.value() * 0.3)
        self.speedLabel.setText(speedStr)

    def gui_events(self, id, value, send=True, toSerial=True, toServer=True):
        self.client.backup(self.variables)
        self.variables[id] = value
        if send:
            self.client.write(self.variables, toSerial=toSerial, toSocketio=toServer)

    def set_to_gui(self, _id:str, value):
        try:
            attribute = getattr(self, _id)
            if isinstance(value, bool):
                attribute.setChecked(value)
                return
            if isinstance(value, int):
                attribute.setValue(value)
                return
        except Exception as e:
            print(e)

    def update_variables(self, data):
        if isinstance(data, dict):
            for _id in data:
                if _id in self.variables:
                    value = data[_id]
                    self.variables[_id] = value
                    self.set_to_gui(_id, value)

    def show_image(self, frame, results):
        pixmap = toPixmap(frame)
        self.image.setPixmap(pixmap)
    
    def record_end(self):
        print("Recording camera ended!")

    def handle_serial_connection_status(self, status):
        self.ledSerial.setChecked(status)

    def recive_data_serial(self, data):
        if isinstance(data, str):
            if data[0] != "$":
                try:
                    variables = json.loads(data)
                    self.update_variables(variables)
                    self.client.write(self.variables, toSerial=False)
                except Exception as e:
                    print(e)
            else:
                print("event: ", data)

    def handle_stop_event_socketio(self):
        #print("stoping mockup")
        self.client.write_to_serial("$stop", toJson=False)

    def handle_socketio_connection_status(self, status):
        self.ledOnline.setChecked(status)
    
    def handle_busy_event(self, status):
        self.ledBusy.setChecked(status)

    def response_to_server(self):
        print("Responding to server ...")
    
    def recive_data_socketio(self, data):
        self.update_variables(data)
        self.client.write(data, toSocketio=False)
    
    def handle_errors(self, error):
        print(error)
    
    def closeEvent(self, e):
        self.client.stop()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Mockup(camera_options, serial_options, socketio_options, extra_options)
    w.show()
    sys.exit(app.exec_())
