from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt
import cv2


def toPixmap(img, size=(320, 240)):
    """convert image from opencv (numpy array) to QPixmap
    :param img: image array

    It returns an image in a Qpixmap format to put it in the GUI
    """
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    h, w, ch = img.shape
    image = QImage(img, w, h, ch * w, QImage.Format_RGB888)
    image = image.scaled(size[0], size[1], Qt.KeepAspectRatio)
    return QPixmap(image)